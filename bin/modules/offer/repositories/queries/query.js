

const MySQL = require('../../../../helpers/databases/mysql/db');
const config = require('../../../../infra/configs/global_config');
const ObjectId = require('mongodb').ObjectId;

const findOneOffer = async (parameter) => {
  const db = new MySQL(config.getDevelopmentDBMySQL());
  const query = `SELECT * FROM offer WHERE id='${parameter.id}';`;
  const recordset = await db.findOne(query);
  return recordset;
};

const findAllOffers = async () => {
  const db = new MySQL(config.getDevelopmentDBMySQL());
  const query = 'SELECT * FROM offer;';
  const recordset = await db.findMany(query);
  return recordset;
};


module.exports = {
  findOneOffer: findOneOffer,
  findAllOffers: findAllOffers
};
