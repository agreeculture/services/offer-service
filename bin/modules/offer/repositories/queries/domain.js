

const nconf = require('nconf');
const rp = require('request-promise');
const model = require('./query_model');
const query = require('./query');
const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../infra/configs/global_config');
const validate = require('validate.js');
const logger = require('../../../../helpers/utils/logger');

class Offer{
  constructor(param){
    this.id = param.id,
    this.buyerId = param.buyerId,
    this.productId = param.productId,
    this.sellerId = param.sellerId,
    this.logistic = param.logistic,
    this.rfq = param.rfq,
    this.status = param.status,
    this.termin = param.termin,
    this.createdAt =  param.createdAt,
    this.updatedAt = param.updatedAt;
  }

  async viewOneOffer(){
    const param = {'id':this.id};
    const result = await query.findOneOffer(param);
    //console.log(result);
    if(result.err){
      return result;
    }
    if(validate.isEmpty(result.data)){
      return wrapper.error('Data Not Found','Please Try Another Input',404);
    }
    result.data = JSON.parse(result.data);
    const data = [result.data];
    let view = model.generalOffer();
    view = data.reduce((accumulator, value) => {
      accumulator.id = value.id;
      accumulator.buyerId = value.buyerId;
      accumulator.productId = value.productId;
      accumulator.sellerId = value.sellerId;
      accumulator.logistic = value.logistic;
      accumulator.rfq = value.rfq;
      accumulator.status = value.status;
      accumulator.termin = value.termin;
      return accumulator;
    }, view);
    return wrapper.data(view);

  }

  async viewAllOffers(){
    const param = {};
    const result = await query.findAllOffers(param);

    if(result.err){
      return result;
    }
    result.data = JSON.parse(result.data);
    return wrapper.data(result.data);

  }

}

module.exports = Offer;
