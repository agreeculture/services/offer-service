

const MySQL = require('../../../../helpers/databases/mariadb/db');
const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../infra/configs/global_config');

const insertOneOffer = async (params) => {
  const db = new MySQL(config.getDevelopmentDBMySQL());
  const queryInsert = 'INSERT INTO offer SET ?';
  const result = await db.query(queryInsert, params);
  //console.log(result);
  return result;
};

const updateOneOffer = async (params, updateQuery) => {
  const id = params.id;
  const db = new MySQL(config.getDevelopmentDBMySQL());
  const query = 'UPDATE offer SET ?';
  const result = await db.command(query);
  return result;
};

const deleteOneOffer = async (params) => {
  const id = params.id;
  const db = new MySQL(config.getDevelopmentDBMySQL());
  const query = `DELETE FROM offer WHERE id='${id}';`;
  const result = await db.command(query);
  return result;
};

module.exports = {
  insertOneOffer: insertOneOffer,
  updateOneOffer: updateOneOffer,
  deleteOneOffer: deleteOneOffer
};
